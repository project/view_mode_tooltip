<?php

/**
 * @file
 * Theme and preprocess functions for View Mode Tooltip
 */

function template_preprocess_view_mode_tooltip(&$variables) {
  $entity_type = $variables['entity_type'];
  $entity = $variables['entity'];
  $view_mode = $variables['view_mode'];
  
  $variables['tooltip'] = drupal_render(field_attach_view($entity_type, $entity, $view_mode));
}
