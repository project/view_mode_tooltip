<?php
/**
 * @file
 * Theme implementation for a View Mode Tooltip
 */ 
?>
<div class="view-mode-tooltip">
  <div class="tooltip">
    <div class="tooltip-content">
      <?php print $tooltip; ?>
    </div>
    <div class="tooltip-arrow-wrap">
      <div class="tooltip-arrow"></div>
    </div>
  </div>
  <div class="element">
    <?php print $element; ?>
  </div>
</div>