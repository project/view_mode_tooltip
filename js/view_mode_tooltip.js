(function($) {
  Drupal.behaviors.view_mode_tooltip = {
    attach: function(context) {
      $('.view-mode-tooltip').mouseenter(function() {
        Drupal.view_mode_tooltip.show($(this));
      });
      
      $('.view-mode-tooltip').mouseleave(function() {
        Drupal.view_mode_tooltip.hide($(this));
      });
    }
  }
  
  Drupal.view_mode_tooltip = {
    'show' : function(el) {
      var tooltip = el.find('.tooltip');
      tooltip
        .css({
          top : el.position().top - tooltip.height() - 10,
          left : (el.position().left - (tooltip.width() / 2)) + (el.width() / 2)
        })
        .fadeIn();
    },
    'hide' : function(el) {
      var tooltip = el.find('.tooltip');
      tooltip.hide()
    }
  }
})(jQuery);
